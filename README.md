# Examen Final

# Día 11/02/2021 Tiempo: 5 horas

- Nota: Cada pregunta se valorará cómo bien o cómo mal (valoraciones intermedias serán excepcionales).
- Nota2: En cada pregunta se especifica si se valora en el examen de diseño o en el de desarrollo.
- Nota3: Para aprobar cada examen hay que obtener una puntuación mínima de 5 puntos en ese examen.
- Nota4: Organice su tiempo. Si no consigue resolver un apartado pase al siguiente. El examen consta de apartados de diseño y de desarrollo que se pueden resolver por separado. Si un apartado depende de otro que no sabe resolver, siempre puede dar una solución que aunque no sea correcta, le permita seguir avanzando.
- Nota5: Para que una solución sea correcta, no sólo hay que conseguir que haga lo que se pide, sino que además todo lo que funcionaba lo tiene que seguir haciendo. La solución debe estar implementada según las prácticas de código limpio explicadas en clase. Debe también atenerse a las buenas práctica en React y JavaScript.
- Nota6: No se permiten modificaciones sustanciales del código que se le suministra en el enunciado, aunque sí puede hacer pequeñas variaciones. Concretamente, en el examen de diseño no se permiten modificar los ficheros "Main.js" y "Course.js" y en el examen de desarrollo no se permite modificar el fichero "services/courses.js" ni el fichero "courses.json". Tampoco se permite utilizar ninguna librería que no esté ya incluida en el fichero "package.json".
- Nota7: Lea completamente el examen antes de empezar y comience por lo que le parezca más fácil.

Pasos previos antes de empezar

- Clone el repositorio del enunciado

```bash
git clone https://gitlab.com/surtich/enunciado-examen-final.git enunciado-examen-final
```

- Vaya al directorio del proyecto

```bash
cd enunciado-examen-final
```

- Configure su usuario de git:

```bash
git config user.name "Sustituya por su nombre y apellidos"
git config user.email "Sustituya por su correo electrónico"
```

- Cree un _branch_ con su nombre y apellidos separados con guiones (no incluya mayúsculas, acentos o caracteres no alfabéticos, excepción hecha de los guiones). Ejemplo:

```bash
    git checkout -b fulanito-perez-gomez
```

- Compruebe que está en la rama correcta:

```bash
    git status
```

- Suba la rama al repositorio remoto:

```bash
    git push origin <nombre-de-la-rama-dado-anteriormente>
```

- Instale las dependencias y arranque la aplicación:

```bash
    yarn install
    yarn start
```

Navegue a [http://localhost:3000](http://localhost:3000)


Debería ver esto:

![Image 0](./enunciado/0.png)


- Dígale al profesor que ya ha terminado para que compruebe que todo es correcto y desconecte la red.

## EXAMEN DE DISEÑO

Nota: En este examen no puede modificar el código HTML, todo debe hacerse con CSS.

#### 1.- Realizar el diseño de las tarjetas con los cursos usando principalmente FlexBox. Debe usar posicionamiento absoluto sólo cuando sea necesario.

El resultado final será:

![Image Final](./enunciado/999.png)

#### 1.1.- (2 puntos diseño) Usando FlexBox, muestre los cursos con el número de columnas que quepan en la pantalla (ver imagen 5.3 para más de dos columnas). Tenga en cuenta márgenes, alineaciones y color de fondo.

![Image 1.1](./enunciado/1.1.png)

#### 1.2.- (1 punto diseño) Cada curso tendrá un degradado de fondo, un sombreado y las esquinas redondeadas. La letra será blanca.

![Image 1.2](./enunciado/1.2.png)

#### 1.3.- (0.5 puntos diseño) La sección de detalles del curso tendrá el fondo que se muestra y estará redondeada.

![Image 1.3](./enunciado/1.3.png)

#### 1.4.- (1 punto diseño) Las fotos tendrán la apariencia que se muestra.

Nota: Verá que cada curso tiene un número de fotos diferente. Para probarlo, no debe modificar el fichero Course.js, sino que debe hacerlo desde la consola de desarrollador.

![Image 1.4](./enunciado/1.4.png)

#### 1.5.- (2 puntos diseño) La sección de detalles tendrá la apariencia de la imagen.

Nota: Observe tamaño de iconos y alineación.

![Image 1.5](./enunciado/1.5.png)

#### 1.6.- (1 punto diseño) El cursor del ratón será de tipo pointer cuando se sitúe encima de la tarjeta de un curso.

Nota: Puede conseguir esto posicionando un elemento "after" encima de la tarjeta.

![Image 1.6](./enunciado/1.6.gif)

#### 1.7.- (0.5 puntos diseño) La tarjeta se desplazará ligeramente cuando se sitúe encima (ver imagen anterior).


### 2.- Implemente las media queries cuando la pantalla se reduzca

![Image 2](./enunciado/2.gif)

#### 2.1.- (1 punto diseño) Al reducir la pantalla se pasará a una columna.

Nota: Esto ya lo debería haber hecho en el apartado 1.1. Aquí se trata de que los márgenes y el espaciado entre componentes sea adecuado.

![Image 2.1](./enunciado/2.1.png)

#### 2.2.- (1 punto diseño) Cuando la pantalla sea aún más pequeña se aumentará el tamaño de las imágenes y se dispondrán cómo se muestra.

![Image 2.2](./enunciado/2.2.png)

## EXAMEN DE DESARROLLO CLIENTE

Importante: Antes de comenzar con este examen, es conveniente que haga un commit de la parte de diseño

```bash
    git add .
    git commit -m "completed exam of design"
```

Se pretende que haga lo que se muestra en el vídeo:

![Image Final](./enunciado/3.gif)

Es decir, se trata de que los cursos se carguen asíncronamente, que haya una paginación y que se puedan borrar cursos.

#### 3.- Carga asíncrona de cursos y autores

Nota: Si no fuera capaz de realizar este apartado, siempre puede leer directamente el fichero courses.json y pasar a los siguientes apartados. También podría crear los cursos estáticamente (en ese caso, no sumaría en este apartado 3).
#### 3.1.- (1 punto desarrollo) Modifique los ficheros Course.js y Main.js para que los cursos se lean asíncronamente del servidor. Para ello dispone de la función ya implementada getAsyncCourses en el fichero "services/courses.js. Los cursos se deben leer una única vez y en el primer renderizado, durante el proceso de carga debe verse el texto "loading".

Nota: Para comprobar que no se cargan los cursos más de una vez, en la consola se imprimen los cursos cada vez que se recuperen del servidor.

![Image 3.1](./enunciado/3.1.png)

#### 3.2.- (2 puntos desarrollo) Muestre los nombres y la foto de los autores de cada curso. Observe que en el fichero courses.json cada curso tiene un array authors con el id de cada autor. Se trata de que llame al servicio getAsyncAuthors para asociar a cada curso los autores que le correspondan y mostrarlos. La carga de autores debe hacerse una única vez junto con los cursos. 

Nota: Observe que los cursos tardan en cargarse 0,5 segundos y los autores 1 segundo. Estos tiempos son una mera prueba, debe asegurare de que antes de que se pinte el curso, los autores están cargados.

Nota 2: La foto del autor es servida cómo estático desde el servidor. Los autores en courses.json tienen un campo photo con el nombre del fichero. Por ejemplo, para acceder a la foto del autor 1, Andrew Ng, debe user esta URL: http://localhost:3001/headshot-andrew-ng.jpg

![Image 3.2](./enunciado/3.2.png)

#### 4.- Borrado de cursos en el cliente.

#### 4.1.- (1 punto desarrollo) Cuando pulse sobre un curso, se pondrá un borde alrededor del mismo. Si pulsa sobre otro curso se quitará el borde del anterior y se seleccionará el pulsado.

![Image 4.1](./enunciado/4.1.png)

#### 4.2.- (1 punto desarrollo) Si vuelve a pulsar sobre un curso seleccionado, éste se borrará del cliente, es decir, que se dejará de mostrar.

#### 4.3.- (1 punto desarrollo) El hueco que ocupaba el curso borrado se mantendrá.

![Image 4.3](./enunciado/4.3.png)


#### 5.- Paginación en el cliente.

#### 5.1.- (0,5 puntos desarrollo) Aunque del servidor se hayan recuperado 8 cursos, sólo se mostrarán los cuatro primeros. Habrá un botón que permitirá cargar el resto.

![Image 5.1](./enunciado/5.1.png)

#### 5.2.- (1 punto desarrollo) Cada vez que se pulse sobre el botón, se cargarán dos cursos más.

![Image 5.2](./enunciado/5.2.png)

#### 5.3.- (0,5 puntos desarrollo) Cuando no haya más cursos que cargar, se ocultará el botón.

![Image 5.3](./enunciado/5.3.png)

#### 5.4.- (2 puntos desarrollo) No se tendrán en cuenta los cursos borrados al calcular el número de cursos mostrados.

![Image 5.4](./enunciado/5.4.gif)


## Para entregar

- Ejecute el siguiente comando para comprobar que está en la rama correcta y ver los ficheros que ha cambiado:

```bash
    git status
```

- Prepare los cambios para que se añadan al repositorio local:

```bash
    git add .
    git commit -m "completed exam"
```

- Compruebe que no tiene más cambios que incluir:

```bash
    git status
```

- Dígale al profesor que va a entregar el examen.

- Conecte la red y ejecute el siguiente comando:

```bash
    git push origin <nombre-de-la-rama>
```

- Abandone el aula en silencio.