import { ReactComponent as CourseIcon } from "../svg/courses.svg";
import { ReactComponent as LevelIcon } from "../svg/level.svg";
import { ReactComponent as AuthorsIcon } from "../svg/authors.svg";

function Course() {
    return (
        <article className="course">
            <div className="course__left">
                <h3 className="course__title">
                    <a href="/" className="course__link">
                        Deep Learning Specialization
                    </a>
                </h3>
                <ul className="course__photos">
                    <li className="course__photo">
                        <img src="img/headshot-andrew-ng.jpg" alt="author" />
                    </li>
                    <li className="course__photo">
                        <img src="img/headshot-kian-katanforoosh.jpg" alt="author" />
                    </li>
                    <li className="course__photo">
                        <img src="img/headshot-younes-bensouda-mourri.jpg" alt="author" />
                    </li>
                </ul>
            </div>
            <div className="course__right">
                <ul className="course__details">
                    <li className="course__detail">
                        <CourseIcon />
                        <div>5 courses</div>
                    </li>
                    <li className="course__detail">
                        <LevelIcon />
                        <div>Intermediate</div>
                    </li>
                    <li className="course__detail">
                        <AuthorsIcon />
                        <div>
                            Andrew Ng, Kian Katanforoosh, Younes Bensouda Mourri
                        </div>
                    </li>
                </ul>
            </div>
        </article>
    )

}

export default Course;