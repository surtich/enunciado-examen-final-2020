import Course from "./Course";

function Main() {
    return (
        <main className="main">
            <Course />
            <Course />
            <Course />
            <Course />
        </main>
    )

}

export default Main;