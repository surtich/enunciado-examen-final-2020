export const API_ENDPOINT = 'http://localhost:3001';

const delayPromise = (milliseconds) => data => new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve(data)
    }, milliseconds);
});

const trace = result => {
    console.log(result);
    return Promise.resolve(result);
}

export const getAsyncCourses = () => fetch(API_ENDPOINT + "/courses")
    .then(response => response.json()).then(delayPromise(500)).then(trace);

export const getAsyncAuthors = () => fetch(API_ENDPOINT + "/authors")
    .then(response => response.json()).then(delayPromise(1000)).then(trace);
